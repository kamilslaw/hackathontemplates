const firebase = require("firebase");
// Required for side-effects
require("firebase/firestore");

const  config = {
  apiKey: "AIzaSyBilgODOFDsTg_8EC6nbLoo2kueAOJhggs",
  authDomain: "hakatun-cebe0.firebaseapp.com",
  databaseURL: "https://hakatun-cebe0.firebaseio.com",
  projectId: "hakatun-cebe0",
  storageBucket: "hakatun-cebe0.appspot.com",
  messagingSenderId: "251228664978"
};
firebase.initializeApp(config);

const db = firebase.firestore();
const fs = require("fs");

let rawData = fs.readFileSync("../no_newline_data.json");
let games = JSON.parse(rawData);

games.forEach(game => {
  db.collection("games").add(game);
});
