﻿using System.Threading.Tasks;
using CRUD.Logic;
using Microsoft.AspNetCore.Mvc;

namespace CRUD.Controllers
{
    [Route("api/")]
    [ApiController]
    public class TabletController : ControllerBase
    {
        [HttpGet(nameof(GetGameStatus))]
        public async Task<IActionResult> GetGameStatus([FromQuery] string gameId)
        {
            var available = await BookingLogic.CheckIfGameIsAvailable(gameId);
            return Ok(new { available });
        }
        
        [HttpPost(nameof(CreateReservationForGame))]
        public async Task<IActionResult> CreateReservationForGame([FromQuery] string gameId, [FromQuery] string userId)
        {
            await CreateReservationLogic.CreateReservationForToday(userId, gameId);
            return Ok();
        }
    }
}