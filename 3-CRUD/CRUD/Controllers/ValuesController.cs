﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace CRUD.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }
        
        [HttpGet(nameof(GetUsers))]
        public async Task<IActionResult> GetUsers()
        {
            var db = FirestoreConfig.GetDb();
            var usersRef = db.Collection("users");
            var snapshot = await usersRef.GetSnapshotAsync();
            
            //Dictionary<string, object> documentDictionary = snapshot.Documents.First().ToDictionary();
            //var someGame = (await ((documentDictionary["favourites"] as List<object>)[0] as DocumentReference).GetSnapshotAsync()).ConvertTo<Game>();
            return Ok((snapshot.Documents.Select(x => (x.Id, x.ConvertTo<User>())).ToList()));
        }
        
        [HttpGet(nameof(GetBookings))]
        public async Task<IActionResult> GetBookings()
        {
            var db = FirestoreConfig.GetDb();
            var usersRef = db.Collection("bookings");
            var snapshot = await usersRef.GetSnapshotAsync();
            
            return Ok((snapshot.Documents.Select(x => (x.Id, x.ConvertTo<Booking>())).ToList()));
        }
    }
}
