﻿using System;
using System.Threading.Tasks;
using CRUD.Logic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace CRUD
{
    [Route("api/")]
    [ApiController]
    public class AppControllerr : ControllerBase
    {
        [HttpPost(nameof(ReportGame))]
        public async Task<IActionResult> ReportGame([FromQuery] string gameId, [FromQuery] string comment)
        {
            await ReportLogic.ReportGame(gameId, comment);
            return Ok();
        }

        [HttpPost(nameof(CloseReservation))]
        public async Task<IActionResult> CloseReservation([FromQuery]string gameId)
        {
            await CloseReservationLogic.Perform(gameId);
            return Ok();
        }

        [HttpPost(nameof(CreateReservation))]
        public async Task<IActionResult> CreateReservation([FromBody] Booking booking)
        {
            await CreateReservationLogic.Perform(booking);
            return Ok();
        }

        [HttpPost(nameof(RemoveReservation))]
        public async Task<IActionResult> RemoveReservation([FromQuery] string bookingId)
        {
            await RemoveReservationLogic.Perform(bookingId);
            return Ok();
        }

        // Get games - pobiera wszystkie gry, z oznaczeniem czy można je rezerwować, albo jeśli łatwiej to tylko oznaczenie i Id
        [HttpGet(nameof(GetGames))]
        public async Task<IActionResult> GetGames([FromQuery] DateTime date)
        {
            var games = await GamesLogic.GetAllGamesWithStatusForDay(date);
            return Ok(games);
        }

        [HttpGet(nameof(GetBookingsForUser))]
        public async Task<IActionResult> GetBookingsForUser([FromQuery] string userId)
        {
            var bookings = await BookingLogic.GetBookingsForUser(userId);
            return Ok(bookings);
        }

        // ręczne odpalanie zadań z crona
        [HttpGet(nameof(TournamentSchedule))]
        public async Task<IActionResult> TournamentSchedule()
        {
            await TournamentScheduler.Execute();
            return Ok();
        }


        [HttpGet(nameof(StartDaySchedule))]
        public async Task<IActionResult> StartDaySchedule()
        {
            await StartDayScheduler.Execute();
            return Ok();
        }

        [HttpGet(nameof(EndDaySchedule))]
        public async Task<IActionResult> EndDaySchedule()
        {
            await EndDayScheduler.Execute();
            return Ok();
        }
    }
}
