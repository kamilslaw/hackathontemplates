﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CRUD.Models;
using CRUD.ViewModels;

namespace CRUD.Logic
{
    public static class BookingLogic
    {
        public static async Task<List<GameForUser>> GetBookingsForUser(string userId)
        {
            var db = FirestoreConfig.GetDb();
            var games = await FirestoreUtils.GetAllGames(db);
            var bookings = await FirestoreUtils.GetBookingsFor7DaysAndUser(db, userId);

            return bookings.Join(games, booking => booking.game, game => game.id, (booking, game) => new GameForUser
                {
                    age = game.age,
                    booking_date = booking.bookingDate,
                    complexity = game.complexity,
                    day_of_week = booking.bookingDate.DayOfWeek.ToString(),
                    description = game.description,
                    game_length = game.game_length,
                    game_id = game.id,
                    image_src = game.image_src,
                    is_primary = booking.isPrimary,
                    players = game.players,
                    rating = game.rating,
                    title = game.title,
                    status = booking.status == BookingStatus.Open
                        ? GameStatus.Closed
                        : booking.status == BookingStatus.Waiting
                            ? GameStatus.Available
                            : GameStatus.Closed,
                    id = booking.id
                })
                .ToList();
        }

        public static async Task<bool> CheckIfGameIsAvailable(string gameId)
        {
            var db = FirestoreConfig.GetDb();
            var bookings = await FirestoreUtils.GetBookingsForUserToday(db, gameId);
            return !bookings.Any(b => b.status == BookingStatus.Open 
                                         || b.status == BookingStatus.Waiting);
        }
    }
}
