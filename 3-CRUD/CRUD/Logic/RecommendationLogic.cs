﻿using System.Collections.Generic;
using CRUD.ViewModels;

namespace CRUD
{
    public static class RecommendationLogic
    {
        public static List<RecommendationModel> Perform()
        {
            return new List<RecommendationModel>
            {
                new RecommendationModel("0sfuAub9h3Dwv6zowBGx", "This game has been borrowed 14 times in last 30 days"),
                new RecommendationModel("2w56gEt1KdKFH0OSeJuZ", "This game has been borrowed 9 times in last 30 days"),
                new RecommendationModel("75RWwKO7GxDA4fbOoIC8", "This game has similiar level of difficulty to the games you have played recently"),
                new RecommendationModel("8AYhAIf3slie7WUJpOh6", "Single game no more than 30 minutes")
            };
        }
    }
}
