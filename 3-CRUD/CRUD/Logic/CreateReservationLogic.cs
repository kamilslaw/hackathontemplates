﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRUD
{
    public static class CreateReservationLogic
    {
        /*
           tworzy obiekt rezerwacji, wykonuje ewentualne veto i powiadomienie o tym vecie, a także obiekt waitings i powiadomienia do tych co lubią grę
         */
        public static async Task Perform(Booking booking)
        {
            var db = FirestoreConfig.GetDb();
            var bookingsRef = db.Collection("bookings");
            var notificationsRef = db.Collection("notifications");
            var waitingsRef = db.Collection("waitings");

            var snapshot = await bookingsRef.GetSnapshotAsync();
            var pendingBookings = snapshot.Documents.Select(x => x.ConvertTo<Booking>().AppendReference(x))
                                             .Where(x => x.game == booking.game && (x.status == BookingStatus.Open || x.status == BookingStatus.Waiting))
                                             .ToList();

            var events = await FirestoreUtils.GetLast30DaysEvents(db, pendingBookings.Select(x => x.user).Distinct().ToList());
            if (pendingBookings.Any() && !pendingBookings.All(x => !x.isPrimary || events.Any(y => y.user == x.user)))
            {
                return;
            }

            Waiting waiting = null;
            if (pendingBookings.Any())
            {
                waiting = (await waitingsRef.GetSnapshotAsync()).Documents.Select(x => x.ConvertTo<Waiting>().AppendReference(x))
                                                                .FirstOrDefault(x => x.game == booking.game);
            }

            var game = await FirestoreUtils.GetGame(db, booking.game);

            foreach (var pendingBooking in pendingBookings)
            {
                pendingBooking.status = BookingStatus.Veto;
                pendingBooking.comment = $"Gra {game.title} została zarezerwowana przez innego użytkownika";
                await pendingBooking.reference.UpdateAsync(pendingBooking.ToFirestoreDict());

                var notification = new Notification
                {
                    msg = $"Gra {game.title} została zarezerwowana przez innego użytkownika",
                    description = $"Gra została zarezerwowana przez innego użytkownika (powód: {(!pendingBooking.isPrimary ? "drugorzędzna rezerwacja" : "jesteś psujem")})",
                    type = NotificationType.Problem,
                    user = pendingBooking.user
                };
                await notificationsRef.AddAsync(notification.ToFirestoreDict());

                if (waiting != null && waiting.bookings.Contains(pendingBooking.id))
                {
                    waiting.bookings.Remove(pendingBooking.id);
                }
            }

            booking.status = BookingStatus.Waiting;
            booking.creationDate = DateTime.UtcNow;
            booking.bookingDate = DateTime.SpecifyKind(booking.bookingDate, DateTimeKind.Utc);
            booking.returnDate = DateTime.SpecifyKind(booking.returnDate, DateTimeKind.Utc);
            booking.id = (await bookingsRef.AddAsync(booking.ToFirestoreDict())).Id;

            if (waiting is null)
            {
                waiting = new Waiting
                {
                    bookings = new List<string> { booking.id },
                    startDate = DateTime.UtcNow,
                    game = booking.game
                };
                await waitingsRef.AddAsync(waiting.ToFirestoreDict());
            }
            else
            {
                waiting.bookings.Add(booking.id);
                await waiting.reference.UpdateAsync(waiting.ToFirestoreDict());
            }
        }

        public static async Task CreateReservationForToday(string userId, string gameId)
        {
            var db = FirestoreConfig.GetDb();

            var bookingsRef = db.Collection("bookings");
            var booking = new Booking
            {
                bookingDate = DateTime.UtcNow,
                creationDate = DateTime.UtcNow,
                game = gameId,
                user = userId,
                isPrimary = true,
                returnDate = DateTime.UtcNow,
                status = BookingStatus.Taken,
                comment = "Gra zarezerwowana"
            };

            await bookingsRef.AddAsync(booking.ToFirestoreDict());
        }
    }
}
