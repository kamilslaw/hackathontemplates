﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace CRUD.Logic
{
    public static class ReportLogic
    {
        public static async Task ReportGame(string gameId, string comment)
        {
            var db = FirestoreConfig.GetDb();
            var events = db.Collection("events");
            var notifications = db.Collection("notifications");
            var bookings = await FirestoreUtils.GetAllBookings(db);

            var lastBooking = bookings.Where(b => b.game == gameId)
                .OrderByDescending(b => b.bookingDate)
                .FirstOrDefault();

            if (lastBooking == null)
            {
                return;
            }

            var user = lastBooking.user;
            lastBooking.status = BookingStatus.Problem;
            await lastBooking.reference.UpdateAsync(lastBooking.ToFirestoreDict());

            var problemEvent = new Event
            {
                booking = lastBooking.id,
                date = DateTime.UtcNow,
                description = comment,
                isAccepted = false,
                user = user
            };
            await events.AddAsync(problemEvent.ToFirestoreDict());

            var notification = new Notification
            {
                user = "ADMIN",
                type = NotificationType.Problem,
                description = $"Użytkownik zgłosił błąd w grze o id: {lastBooking.game}",
                msg = $"Użytkownik zgłosił błąd w grze o id: {lastBooking.game}"
            };
            await notifications.AddAsync(notification.ToFirestoreDict());
        }
    }
}
