﻿using System.Linq;
using System.Threading.Tasks;

namespace CRUD
{
    public static class CloseReservationLogic
    {
        public static async Task Perform(string gameId)
        {
            var db = FirestoreConfig.GetDb();
            var bookingsRef = db.Collection("bookings");
            var booking = (await bookingsRef.GetSnapshotAsync()).Documents.Select(x => x.ConvertTo<Booking>().AppendReference(x))
                                                                          .FirstOrDefault(x => x.status == BookingStatus.Taken && x.game == gameId);
            if (booking is null)
            {
                return;
            }

            booking.status = BookingStatus.Ended;
            await booking.reference.UpdateAsync(booking.ToFirestoreDict());
        }
    }
}
