﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CRUD.Models;
using CRUD.ViewModels;

namespace CRUD.Logic
{
    public static class GamesLogic
    {
        public static async Task<List<GameWithStatus>> GetAllGamesWithStatusForDay(DateTime date)
        {
            var db = FirestoreConfig.GetDb();
            var games = await FirestoreUtils.GetAllGames(db);
            var bookings = await FirestoreUtils.GetBookingsForDay(db, date);
            var userIds = bookings
                .Select(x => x.user)
                .ToList();

            var events = await FirestoreUtils.GetLast30DaysEvents(db, userIds);
            var bannedUsers = events.Select(e => e.user);

            var bookingsWithoutBannedUsers = bookings.Where(b => !bannedUsers.Contains(b.user));

            var recommendations = RecommendationLogic.Perform();

            return games.GroupJoin(
                    bookingsWithoutBannedUsers.Where(b =>
                        b.status == BookingStatus.Open || b.status == BookingStatus.Waiting), game => game.id,
                    booking => booking.game, (game, booking) => new GameWithStatus
                    {
                        complexity = game.complexity,
                        age = game.age,
                        description = game.description,
                        game_length = game.game_length,
                        image_src = game.image_src,
                        players = game.players,
                        rating = game.rating,
                        id = game.id,
                        title = game.title,
                        status = !booking.Any()
                            ? GameStatus.Open
                            : booking.First().status == BookingStatus.Waiting
                                ? GameStatus.Available
                                : GameStatus.Closed,
                        is_recommendation = recommendations.FirstOrDefault(x => x.GameId == game.id) != null,
                        recommendation = recommendations.FirstOrDefault(x => x.GameId == game.id)?.Comment
                    })
                .ToList();
        }
    }
}
