﻿using System.Linq;
using System.Threading.Tasks;

namespace CRUD
{
    public static class RemoveReservationLogic
    {
        // usuwamy rezerwację, edytujemy waiting jeśli istnieje, przesuwamy inny booking notPrimary na primary, jeśli jest
        public static async Task Perform(string bookingId)
        {
            var db = FirestoreConfig.GetDb();
            var bookingsRef = db.Collection("bookings");
            var waitingsRef = db.Collection("waitings");

            var pendingBookings = (await bookingsRef.GetSnapshotAsync()).Documents.Select(x => x.ConvertTo<Booking>().AppendReference(x))
                                                                        .Where(x => x.id == bookingId || x.status == BookingStatus.Open || x.status == BookingStatus.Waiting)
                                                                        .ToList();

            var currentBooking = pendingBookings.First(x => x.id == bookingId);
            await currentBooking.reference.DeleteAsync();

            if (currentBooking.isPrimary && pendingBookings.Any(x => !x.isPrimary))
            {
                var upgradedBooking = pendingBookings.First(x => !x.isPrimary);
                upgradedBooking.isPrimary = true;
                await upgradedBooking.reference.UpdateAsync(upgradedBooking.ToFirestoreDict());
            }

            var waiting = (await waitingsRef.GetSnapshotAsync()).Documents.Select(x => x.ConvertTo<Waiting>().AppendReference(x))
                                                                .FirstOrDefault(x => x.bookings.Contains(bookingId));
            if (waiting != null)
            {
                waiting.bookings.Remove(bookingId);
                await waiting.reference.UpdateAsync(waiting.ToFirestoreDict());
            }
        }
    }
}
