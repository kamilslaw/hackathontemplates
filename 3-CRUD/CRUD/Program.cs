﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace CRUD
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
#if DEBUG
                .UseUrls("http://localhost:5000")
#endif
                .UseStartup<Startup>();
    }
}
