﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Google.Cloud.Firestore;

namespace CRUD
{
    public static class FirestoreUtils
    {
        public static async Task<Game> GetGame(FirestoreDb db, string gameId)
        {
            var gameRef = db.Collection("games").Document(gameId);
            var snapshot = await gameRef.GetSnapshotAsync();
            return snapshot.ConvertTo<Game>().AppendReference(snapshot);
        }

        public static async Task<User> GetUser(FirestoreDb db, string userId)
        {
            var userRef = db.Collection("users").Document(userId);
            var snapshot = await userRef.GetSnapshotAsync();
            return snapshot.ConvertTo<User>().AppendReference(snapshot);
        }

        public static async Task<User> GetAdmin(FirestoreDb db)
        {
            var userRef = db.Collection("users").Document("ADMIN");
            var snapshot = await userRef.GetSnapshotAsync();
            return snapshot.ConvertTo<User>().AppendReference(snapshot);
        }

        public static async Task<List<Booking>> GetBookings(FirestoreDb db, List<string> bookingIds)
        {
            var bookingsRef = db.Collection("bookings");
            var snapshot = await bookingsRef.GetSnapshotAsync();
            var bookings = snapshot.Documents.Select(x => x.ConvertTo<Booking>().AppendReference(x))
                                          .Where(x => bookingIds.Contains(x.id))
                                          .ToList();
            return bookings;
        }

        public static async Task<List<User>> GetBookingsUsers(FirestoreDb db, List<Booking> bookings)
        {
            var usersRef = db.Collection("users");
            var snapshot = await usersRef.GetSnapshotAsync();
            var users = snapshot.Documents.Select(x => x.ConvertTo<User>().AppendReference(x))
                                          .Where(x => bookings.Any(y => y.user == x.id))
                                          .ToList();
            return users;
        }

        public static async Task<List<Event>> GetLast30DaysEvents(FirestoreDb db, List<string> userIds)
        {
            var eventRef = db.Collection("events");
            var snapshot = await eventRef.GetSnapshotAsync();
            var events = snapshot.Documents.Select(x => x.ConvertTo<Event>().AppendReference(x))
                                           .Where(x => x.isAccepted && (DateTime.UtcNow - x.date).TotalDays <= 30)
                                           .ToList();
            return events;
        }

        public static async Task<List<Game>> GetAllGames(FirestoreDb db)
        {
            var gameRef = db.Collection("games");
            var snapshot = await gameRef.GetSnapshotAsync();
            return snapshot.Select(s => s.ConvertTo<Game>().AppendReference(s)).ToList();
        }

        public static async Task<List<Booking>> GetBookingsForDay(FirestoreDb db, DateTime date)
        {
            var bookings = db.Collection("bookings");
            var snapshot = await bookings.GetSnapshotAsync();
            return snapshot
                .Select(b => b.ConvertTo<Booking>().AppendReference(b))
                .Where(b => b.bookingDate.Date == date.Date)
                .ToList();
        }

        public static async Task<List<Booking>> GetBookingsFor7DaysAndUser(FirestoreDb db, string userId)
        {
            var bookings = db.Collection("bookings");
            var snapshot = await bookings.GetSnapshotAsync();
            var today = DateTime.UtcNow;
            var in7Days = DateTime.UtcNow.AddDays(7);
            return snapshot
                .Select(b => b.ConvertTo<Booking>().AppendReference(b))
                .Where(b => b.status != BookingStatus.Veto && b.status != BookingStatus.Ended
                            && b.bookingDate.Date >= today.Date
                            && b.bookingDate.Date <= in7Days.Date 
                            && b.user == userId)
                .ToList();
        }

        public static async Task<List<Booking>> GetBookingsForUserToday(FirestoreDb db, string gameId)
        {
            var bookings = db.Collection("bookings");
            var snapshot = await bookings.GetSnapshotAsync();
            var today = DateTime.UtcNow;
            return snapshot
                .Select(b => b.ConvertTo<Booking>().AppendReference(b))
                .Where(b => b.bookingDate.Date == today.Date && b.game == gameId)
                .ToList();
        }

        public static async Task<List<Booking>> GetAllBookings(FirestoreDb db)
        {
            var bookings = db.Collection("bookings");
            var snapshot = await bookings.GetSnapshotAsync();
            var today = DateTime.UtcNow;
            return snapshot
                .Select(b => b.ConvertTo<Booking>().AppendReference(b))
                .ToList();
        }
    }
}
