﻿using System;
using Google.Cloud.Firestore;
using Newtonsoft.Json;

namespace CRUD
{
    [FirestoreData]
    public class Event
    {
        [JsonIgnore]
        public DocumentReference reference;

        public string id;

        public Event AppendReference(DocumentSnapshot reference)
        {
            this.reference = reference.Reference;
            this.id = reference.Id;
            return this;
        }

        [FirestoreProperty]
        public DateTime date { get; set; }
        [FirestoreProperty]
        public string description { get; set; }
        [FirestoreProperty]
        public bool isAccepted { get; set; }

        [FirestoreProperty]
        public string booking { get; set; }
        [FirestoreProperty]
        public string user { get; set; }
    }
}
