﻿using Google.Cloud.Firestore;
using Newtonsoft.Json;

namespace CRUD
{
    public enum NotificationType
    {
        Info = 0,
        Problem = 1
    }

    [FirestoreData]
    public class Notification
    {
        [JsonIgnore]
        public DocumentReference reference;

        public string id;

        public Notification AppendReference(DocumentSnapshot reference)
        {
            this.reference = reference.Reference;
            this.id = reference.Id;
            return this;
        }

        [FirestoreProperty]
        public string msg { get; set; }
        [FirestoreProperty]
        public string description { get; set; }
        [FirestoreProperty]
        public NotificationType type { get; set; }

        [FirestoreProperty]
        public string user { get; set; }
    }
}
