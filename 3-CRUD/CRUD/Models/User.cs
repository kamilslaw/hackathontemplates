﻿using System.Collections.Generic;
using Google.Cloud.Firestore;
using Newtonsoft.Json;

namespace CRUD
{
    [FirestoreData]
    public class User
    {
        [JsonIgnore]
        public DocumentReference reference;

        public string id;

        public User AppendReference(DocumentSnapshot reference)
        {
            this.reference = reference.Reference;
            this.id = reference.Id;
            return this;
        }

        [FirestoreProperty]
        public string name { get; set; }

        [FirestoreProperty]
        public List<string> favourites { get; set; }
    }
}
