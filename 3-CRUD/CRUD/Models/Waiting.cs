﻿using System;
using System.Collections.Generic;
using Google.Cloud.Firestore;
using Newtonsoft.Json;

namespace CRUD
{
    [FirestoreData]
    public class Waiting
    {
        [JsonIgnore]
        public DocumentReference reference;

        public string id;

        public Waiting AppendReference(DocumentSnapshot reference)
        {
            this.reference = reference.Reference;
            this.id = reference.Id;
            return this;
        }

        [FirestoreProperty]
        public DateTime startDate { get; set; }

        [FirestoreProperty]
        public string game { get; set; }
        [FirestoreProperty]
        public List<string> bookings { get; set; }
    }
}
