﻿using System;
using Google.Cloud.Firestore;
using Newtonsoft.Json;

namespace CRUD
{
    public enum BookingStatus
    {
        Waiting = 0,
        Open = 1,
        Taken = 2,
        Veto = 3,
        Ended = 4,
        Lost = 5,
        BlockedByAdmin = 6,
        Problem = 7
    }

    [FirestoreData]
    public class Booking
    {
        [JsonIgnore]
        public DocumentReference reference;

        public string id;

        public Booking AppendReference(DocumentSnapshot reference)
        {
            this.reference = reference.Reference;
            this.id = reference.Id;
            return this;
        }

        [FirestoreProperty]
        public DateTime bookingDate { get; set; }
        [FirestoreProperty]
        public string comment    { get; set; }
        [FirestoreProperty]
        public DateTime creationDate { get; set; }
        [FirestoreProperty]
        public bool isPrimary { get; set; }
        [FirestoreProperty]
        public DateTime returnDate { get; set; }
        [FirestoreProperty]
        public BookingStatus status { get; set; }

        [FirestoreProperty]
        public string game { get; set; }
        [FirestoreProperty]
        public string user { get; set; }
    }
}
