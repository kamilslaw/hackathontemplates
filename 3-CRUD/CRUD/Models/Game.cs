﻿using System.Collections.Generic;
using Google.Cloud.Firestore;
using Newtonsoft.Json;

namespace CRUD
{
    [FirestoreData]
    public class Game
    {
        [JsonIgnore]
        public DocumentReference reference;

        public string id;

        public Game AppendReference(DocumentSnapshot reference)
        {
            this.reference = reference.Reference;
            this.id = reference.Id;
            return this;
        }

        [FirestoreProperty]
        public string age { get; set; }
        [FirestoreProperty]
        public string title { get; set; }
        [FirestoreProperty]
        public string complexity { get; set; }
        [FirestoreProperty]
        public string description { get; set; }
        [FirestoreProperty]
        public string game_length { get; set; }
        [FirestoreProperty]
        public string image_src { get; set; }
        [FirestoreProperty]
        public string players { get; set; }
        [FirestoreProperty]
        public string rating { get; set; }
    }
}
