﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Google.Apis.Auth.OAuth2;
using Google.Cloud.Firestore;
using Google.Cloud.Firestore.V1;
using Grpc.Auth;
using Grpc.Core;

namespace CRUD
{
    public static class FirestoreConfig
    {
        public static FirestoreDb GetDb()
        {
            GoogleCredential cred = GoogleCredential.FromFile(Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "credentials.json"));
            Channel channel = new Channel(FirestoreClient.DefaultEndpoint.Host,
                          FirestoreClient.DefaultEndpoint.Port,
                          cred.ToChannelCredentials());
            FirestoreClient client = FirestoreClient.Create(channel);

            return FirestoreDb.Create(projectId: "hakatun-cebe0", client: client);
        }

        public static Dictionary<string, object> ToFirestoreDict<T>(this T model)
        {
            return typeof(T).GetProperties()
                .Where(prop => Attribute.IsDefined(prop, typeof(FirestorePropertyAttribute)))
                .ToDictionary(
                    prop => prop.Name,
                    prop => prop.GetValue(model, null)
                );
        }
    }
}

// https://firebase.google.com/docs/firestore/quickstart
// https://googleapis.github.io/google-cloud-dotnet/docs/Google.Cloud.Firestore/userguide.html
// https://github.com/googleapis/google-cloud-dotnet/issues/2444
