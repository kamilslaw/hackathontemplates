﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Google.Type;
using Hangfire;
using Microsoft.IdentityModel.Tokens;
using DayOfWeek = System.DayOfWeek;

namespace CRUD
{
    /*
      Raz dziennie, o 10:00 rano:
      Dla każdego wypożyczenia, które jest Taken:
        - zmień status na Problem
        - utwórz notification dla admina i dla tego usera o opóźnieniu
        - utwórz Event, który ma isAccepted na false

        Jeżeli ktoś nie oddał książki ale ma jeszcze na kolejny dzień to zamykamy jak gdyby nigdy nic, czyli wszystko ok
    */
    public static class StartDayScheduler
    {
        public static readonly string CRON = Cron.Daily(10, 00);

        public static async Task Execute()
        {
//            if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday
//                || DateTime.Now.DayOfWeek == DayOfWeek.Sunday)
//            {
//                return;
//            }

            var db = FirestoreConfig.GetDb();
            var usersRef = db.Collection("bookings");
            var snapshot = await usersRef.GetSnapshotAsync();

            var groupedBookings = snapshot.Documents
                .Select(x => x.ConvertTo<Booking>().AppendReference(x))
                .GroupBy(x => new {x.game, x.user})
                .ToList();
            foreach (var bookings in groupedBookings)
            {
                foreach (var booking in bookings)
                {
                    if (booking.bookingDate.Date.AddDays(1) == DateTime.UtcNow.Date && booking.status == BookingStatus.Taken ||
                        (DateTime.UtcNow.DayOfWeek == DayOfWeek.Monday &&
                         booking.bookingDate.Date.AddDays(3) == DateTime.UtcNow.Date && booking.status == BookingStatus.Taken))
                    {
                        if (bookings.Any(b => b.bookingDate.Date == DateTime.UtcNow.Date && b.status == BookingStatus.Open))
                        {
                            booking.status = BookingStatus.Ended;
                            await booking.reference.UpdateAsync(booking.ToFirestoreDict());
                            continue;
                        }

                        booking.status = BookingStatus.Problem;
                        await booking.reference.UpdateAsync(booking.ToFirestoreDict());

                        var game = await FirestoreUtils.GetGame(db, booking.game);
                        var user = await FirestoreUtils.GetUser(db, booking.user);
                        var admin = await FirestoreUtils.GetAdmin(db);

                        var collection = db.Collection("notifications");
                        var notification = new Notification
                        {
                            msg = $"Nie oddałeś gry {game.title}",
                            description = $"Gra {game.title} nie została oddana, a była zarezerwowana {booking.bookingDate.ToString("dd.MM.yyyy")}",
                            type = NotificationType.Problem,
                            user = booking.user
                        };
                        await collection.AddAsync(notification.ToFirestoreDict());
                        
                        var adminNotification = new Notification
                        {
                            msg = $"Użytkownik: {user.name} nie oddał gry: {game.title} zarezerwowanej na dzień {booking.bookingDate.ToString("dd.MM.yyyy")}",
                            description = $"Gra {game.title} nie została oddana przez {user.name}, a była zarezerwowana {booking.bookingDate.ToString("dd.MM.yyyy")}",
                            type = NotificationType.Problem,
                            user = admin.id
                        };
                        await collection.AddAsync(adminNotification.ToFirestoreDict());

                        var eventCollection = db.Collection("events");
                        var problemEvent = new Event
                        {
                            booking = booking.id,
                            date = DateTime.UtcNow,
                            isAccepted = false,
                            user = user.id,
                            description = $"Gra {game.title} nie została oddana przez {user.name}, a była zarezerwowana dnia {booking.bookingDate.ToString("dd.MM.yyyy")}"
                        };
                        await eventCollection.AddAsync(problemEvent.ToFirestoreDict());
                    }
                }
            }
        }
    }
}
