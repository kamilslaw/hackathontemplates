﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Google.Cloud.Firestore;
using Hangfire;

namespace CRUD
{
    /*
      Co minutę:
      sprawdza czy są jakieś trwające requesty na wypożyczenie (kolekcja waitings), dla każdego który zaczął się pół godziny temu:
        - wybierz zwycięzcę: na podstawie przewinień usera, jeśli nie ma żadnych, to wybieramy losowo
        - zmień status wypożyczenia dla usera który wygrał i pozostałych
        - usuń obiekt waitings
        - wyślij powiadomienia do wszystkich (nowy obiekt w kolekcji notifications) - do zwyciescy info, do przegranych danger
    */
    public static class TournamentScheduler
    {
        public static readonly string CRON = Cron.Minutely();

        public static async Task Execute()
        {
            if (DateTime.Now.Hour == 17 && DateTime.Now.Minute == 00)
            {
                return;
            }

            var db = FirestoreConfig.GetDb();
            var waitingsRef = db.Collection("waitings");
            var snapshot = await waitingsRef.GetSnapshotAsync();
            var waitings = snapshot.Documents.Select(x => x.ConvertTo<Waiting>().AppendReference(x))
                                             .Where(x => (DateTime.UtcNow - x.startDate).TotalMinutes >= 30 || (DateTime.Now.Hour == 16 && DateTime.Now.Minute == 59))
                                             .ToList();
            
            foreach (var waiting in waitings)
            {
                var game = await FirestoreUtils.GetGame(db, waiting.game);
                var bookings = await FirestoreUtils.GetBookings(db, waiting.bookings);
                var users = await FirestoreUtils.GetBookingsUsers(db, bookings);
                if (users.Count == 1)
                {
                    await SetWinner(db, users[0], bookings, game);
                }
                else
                {
                    var events = await FirestoreUtils.GetLast30DaysEvents(db, users.Select(x => x.id).ToList());
                    var random = new Random();
                    users = users.OrderBy(x => events.Any(y => y.user == x.id)).ThenBy(_ => random.Next()).ToList();
                    await SetWinner(db, users[0], bookings, game);
                    foreach (var user in users.Skip(1).ToList()) await SetLoser(db, user, bookings, game);
                }

                await waiting.reference.DeleteAsync();
            }
        }

        private static async Task SetWinner(FirestoreDb db, User user, List<Booking> bookings, Game game)
        {
            var booking = bookings.First(x => x.user == user.id);
            booking.status = BookingStatus.Open;
            await booking.reference.UpdateAsync(booking.ToFirestoreDict());

            var collection = db.Collection("notifications");
            var notification = new Notification
            {
                msg = $"Zarezerwowałeś grę {game.title}",
                description = $"Proces rezerwacji na dzień {booking.bookingDate.ToString("dd.MM.yyyy")} został zamknięty",
                type = NotificationType.Info,
                user = user.id
            };
            await collection.AddAsync(notification.ToFirestoreDict());
        }

        private static async Task SetLoser(FirestoreDb db, User user, List<Booking> bookings, Game game)
        {
            var booking = bookings.First(x => x.user == user.id);
            booking.status = BookingStatus.Lost;
            await booking.reference.UpdateAsync(booking.ToFirestoreDict());

            var collection = db.Collection("notifications");
            var notification = new Notification
            {
                msg = $"Rezerwacja {game.title} nie udała się",
                description = $"Proces rezerwacji na dzień {booking.bookingDate.ToString("dd.MM.yyyy")} został zamknięty - gra trafiła do innego użytkownika",
                type = NotificationType.Problem,
                user = user.id
            };
            await collection.AddAsync(notification.ToFirestoreDict());
        }
    }
}
