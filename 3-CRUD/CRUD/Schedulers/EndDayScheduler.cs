﻿using System.Linq;
using System.Threading.Tasks;
using Hangfire;

namespace CRUD
{
    /*
      Raz dziennie, o 17:00:
      zmień status każdego wypożyczenia, które jest Open lub Waiting na Taken
    */
    public static class EndDayScheduler
    {
        public static readonly string CRON = Cron.Daily(17, 00);

        public static async Task Execute()
        {
            var db = FirestoreConfig.GetDb();
            var bookingsRef = db.Collection("bookings");
            var snapshot = await bookingsRef.GetSnapshotAsync();
            
            var bookings = snapshot.Documents.Select(x => x.ConvertTo<Booking>().AppendReference(x))
                                             .Where(x => x.status == BookingStatus.Open || x.status == BookingStatus.Waiting)
                                             .ToList();

            var collection = db.Collection("notifications");
            foreach (var booking in bookings)
            {
                booking.status = BookingStatus.Taken;
                await booking.reference.UpdateAsync(booking.ToFirestoreDict());
            }

            foreach (var lookup in bookings.ToLookup(x => x.user))
            {
                var notification = new Notification
                {
                    msg = $"Nie zapomnij o dzisiejszych rezerwacjach",
                    description = $"Ilość pożyczonych gier: {lookup.Count()}",
                    type = NotificationType.Info,
                    user = lookup.Key
                };
                await collection.AddAsync(notification.ToFirestoreDict());
            }
        }
    }
}
