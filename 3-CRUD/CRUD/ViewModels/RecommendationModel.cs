﻿namespace CRUD.ViewModels
{
    public class RecommendationModel
    {
        public RecommendationModel(string gameId, string comment)
        {
            GameId = gameId;
            Comment = comment;
        }

        public string GameId { get; set; }
        public string Comment { get; set; }
    }
}
