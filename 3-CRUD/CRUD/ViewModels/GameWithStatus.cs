﻿using System.Collections.Generic;
using CRUD.Models;

namespace CRUD.ViewModels
{
    public class GameWithStatus
    {
        public string id;
        public string title { get; set; }
        public string age { get; set; }
        public string complexity { get; set; }
        public string description { get; set; }
        public string game_length { get; set; }
        public string image_src { get; set; }
        public string players { get; set; }
        public string rating { get; set; }
        public GameStatus status { get; set; }
        public bool is_recommendation { get; set; }
        public string recommendation { get; set; }
    }
}
