﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CRUD.Models;

namespace CRUD.ViewModels
{
    public class GameForUser
    {
        public string game_id;
        public string title { get; set; }
        public string age { get; set; }
        public string complexity { get; set; }
        public string description { get; set; }
        public string game_length { get; set; }
        public string image_src { get; set; }
        public string players { get; set; }
        public string rating { get; set; }
        public GameStatus status { get; set; }
        public DateTime booking_date { get; set; }
        public bool is_primary { get; set; }
        public string day_of_week { get; set; }
        public string id { get; set; }
    }
}
