# Wymagane rzeczy na komputerze
- Windows albo Linux
- npm
- .NET Core 2.2 SDK (podstawowe komendy: *dotnet build* i *dotnet run*)
- Klient postgresa - **pgAdmin**, ewentualnie **HeidiSQL** (12MB, portable)

# Dane logowania i adresy
- **Postgres**: 
  - serwer: **176.107.131.163**, login: **root**
  - baza: **11.2**, user: **postgres**, hasło: **12345678**, port: **5432**
  - pozostałe ustawienia bazy: https://hub.docker.com/_/postgres?tab=description
- **CouchDB** (baza danych noSQL, opiera się o JSONy):
  - serwer: **176.107.131.163**, login: **root**
  - port: **5984**, panel graficzny: http://176.107.131.163:5984/_utils/
  - z pozycji panelu można utworzyć nowe bazy danych, a także podglądać dane
- **Firebase**
  - identyfikator projektu: **hackathon-88f9d**, lokalizacja Cloud Firestore: **nam5 (us-central)**, klucz interfejsu Web API: **AIzaSyB_H8evRjqZGapIOh7gXS4cs-kuwRfRRS4**, nazwa widoczna publicznie: **project-361917476249**


# Projekty
1. **CouchDB** - aplikacja konsolowa, .NET Core 2.2 - insert, update, pobieranie filtrowanych danych, pobieranie po Id, mapowanie z JSON na obiekty


2. **PostgreSQL** - aplikacja konsolowa, .NET Core 2.2 - insert, select, bez ORM - jedynie prosty connector i ręcznie pisany SQL

3. **CRUD** - REST API, .NET Core 2.2, ORM - Entity Framework, Swagger, PostgreSQL
    - adres API: http://localhost:50397 (lub http://localhost:5000 jeśli odpalamy z konsoli)
    - adres swaggera (wszystkie dostępne akcje + możliwość klikania): http://localhost:50397/swagger
    - jeśli stworzymy nową aplikacją korzystającą z postgresa i EF, to przed odpaleniem i utworzenie modelów i contextu, możemy użyć skryptu do wygenerowania tego za nas:
   *dotnet ef dbcontext scaffold "Server=176.107.131.163;User Id=postgres;Password=12345678;Database=postgres;" Npgsql.EntityFrameworkCore.PostgreSQL*

4. **Firebase** - Realtime database, HTML, czat
    - plik HTML, z automatycznym odswieżaniem na żywo

5. **EmguCV** - WinForms, .NET Framework 4.6.2, wykrywanie ruchu na kamerce z zaznaczaniem
    - prosta aplikacja wzięta z exampli
    - działa tylko na Windows
    - po zbudowaniu, należy skopiować plik *cvextern.dll* do folderu *bin*


# Inne
- https://github.com/ziyasal/FireSharp/ - biblioteka Firebase dla .NET
- https://sourceforge.net/projects/emgucv/ - link do instalatora EmguCV - wrzuca do folderu *C:\EmguCV* DLLki oraz przykładowe aplikacje