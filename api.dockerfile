FROM microsoft/dotnet:latest
USER root

RUN apt-get update 
RUN apt-get install -y --no-install-recommends apt-utils

RUN mkdir app
COPY ./ app/

WORKDIR /app/publish/

EXPOSE 80

CMD dotnet CRUD.dll