Adres serwera: http://nosql.kis.agh.edu.pl:5984/
Interfejs graficzny: http://nosql.kis.agh.edu.pl:5984/_utils/

############################################################################
Exercise 1
Use your favorite text editor to create at least 2 JSON documents for each of the categories below (6 documents total).
1. announcement: date, subject,
2. store inventory: item name, item description, item price,
3. message: body, sender, recipient, date.

######################################
{"date" : "2018-03-14", "subject": "We need witcher"}
{"date" : "2018-03-15", "subject": "Fun attachment"}
{"item_name" : "book", "item_description": "red book", "item_price": 34.23}
{"item_name" : "pencil", "item_description": "some pencil description", "item_price": 0.99}
{"body" : "HELLO WORLD!", "sender": "adam@adam.com", "recipient": "john@gmail.com", "date" : "2018-03-14"}
{"body" : "HELLO!\nAre you here?", "sender": "ad_234@outlook.com", "recipient": "john2@gmail.com", "date" : "2018-03-15"}



############################################################################
Exercise 2
Create your own CouchDB database at: http://nosql.kis.agh.edu.pl:5984/ . The database name should consist of your initials and last name.
Insert the above documents into your database.

######################################
curl -X PUT http://nosql.kis.agh.edu.pl:5984/kgebarowski
curl -X POST http://nosql.kis.agh.edu.pl:5984/kgebarowski -H "Content-Type: application/json" -d #OBJECT FROM EXERCISE 1 HERE#



############################################################################
Exercise 3
Create a view which returns announcement subjects ordered by date.

######################################
curl -X PUT http://nosql.kis.agh.edu.pl:5984/kgebarowski/_design/lab1 -H "Content-Type: application/json" -d @views.json

plik views.json:
{
	"_id":"_design/lab1",
	"language": "javascript",
	"views":
	{
		"subject": {
			"map": "function(doc) {
						if (doc.subject !== undefined)
							emit(doc.date, doc.subject) }"
		}
	}
}

curl -X GET http://nosql.kis.agh.edu.pl:5984/kgebarowski/_design/lab1/_view/subject



############################################################################
Exercise 4
Create a view which counts letter occurrences in all documents (case insensitive). Use Futon to streamline the process of creating the view: http://nosql.kis.agh.edu.pl:5984/_utils
Expected result:
{"rows":[
{"key":"a","value":51},
{"key":"b","value":56},
{"key":"c","value":17},
{"key":"d","value":59},
{"key":"e","value":28},
{"key":"f","value":42},
{"key":"g","value":17},
{"key":"h","value":5},
{"key":"i","value":20},
{"key":"j","value":11},
{"key":"k","value":11},
{"key":"l","value":6},
{"key":"m","value":9},
{"key":"n","value":8},
{"key":"o","value":19},
{"key":"p","value":5},
{"key":"q","value":7},
{"key":"r","value":5},
{"key":"s","value":16},
{"key":"t","value":9},
{"key":"u","value":7},
{"key":"v","value":6},
{"key":"w","value":6},
{"key":"y","value":6},
{"key":"z","value":2}
]}
Hint:  emit() function could be used more than once in the map function.

######################################
Rozwiązanie wstawić jako kolejny widok do skryptu w zadaniu wyżej ("letters")
MAP:
function(doc) {
	for (prop in doc) {
		if (prop !== "_id" && prop !== "_rev" && !isNaN(doc[prop]) && typeof(doc[prop]) !== typeof(true)) {
			var letters = doc[prop].toLowerCase().match(/[a-z]/g);
			if (letters !== null) letters.forEach(function(x) { emit(x, 1); })
		}
	}
}

REDUCE:
function(key, values) {
	return sum(values);
}

curl -X GET http://nosql.kis.agh.edu.pl:5984/kgebarowski/_design/lab1/_view/letters?group="true"
