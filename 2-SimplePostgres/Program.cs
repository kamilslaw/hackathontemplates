﻿using System;
using Npgsql;
using NpgsqlTypes;

namespace PostgreSqlConnector
{
    // http://www.npgsql.org/doc/
    class Program
    {
        static void Main(string[] args)
        {
            var conn = new NpgsqlConnection("Server=176.107.131.163;User Id=postgres;Password=12345678;Database=postgres;");
            conn.Open();

            using (var cmd = new NpgsqlCommand())
            {
                cmd.Connection = conn;
                cmd.CommandText = "INSERT INTO \"TestTable1\" (\"Name\", \"IsOk\", \"Price\") VALUES (@p1, @p2, @p3)";
                cmd.Parameters.AddWithValue("p1", "Hello world " + DateTime.Now.ToLongTimeString());
                cmd.Parameters.AddWithValue("p2", NpgsqlDbType.Bit, true);
                cmd.Parameters.AddWithValue("p3", 33.55M + (DateTime.Now.Second / 3.0M));
                cmd.ExecuteNonQuery();
            }

            var command = new NpgsqlCommand("SELECT * FROM \"TestTable1\"", conn);
            var dr = command.ExecuteReader();
            while (dr.Read())
                Console.Write("ID: {0}\tNAME: {1}\tISOK: {2}\tPRICE: {3}{4}", dr.GetInt32(0), dr[1], dr.GetBoolean(2), dr.GetDecimal(3), Environment.NewLine);

            conn.Close();

            Console.ReadLine();
        }
    }
}
