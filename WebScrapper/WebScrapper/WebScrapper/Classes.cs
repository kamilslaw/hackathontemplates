﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebScrapper
{
    public class Option
    {
        public int value { get; set; }
        public string title { get; set; }
    }

    public class Itemdata
    {
        public string datatype { get; set; }
        public string fieldname { get; set; }
        public string title { get; set; }
        public bool primaryname { get; set; }
        public bool required { get; set; }
        public bool unclickable { get; set; }
        public bool fullcredits { get; set; }
        public string subtype { get; set; }
        public string keyname { get; set; }
        public bool? alternate { get; set; }
        public string createposttext { get; set; }
        public string posttext { get; set; }
        public string createtitle { get; set; }
        public string table { get; set; }
        public List<Option> options { get; set; }
        public bool? adminonly { get; set; }
        public string validatemethod { get; set; }
        public bool? nullable { get; set; }
        public string other_objecttype { get; set; }
        public string other_subtype { get; set; }
        public string linktype { get; set; }
        public string self_prefix { get; set; }
        public string titlepl { get; set; }
        public string lookup_subtype { get; set; }
        public bool? showall_ctrl { get; set; }
        public bool? loadlinks { get; set; }
        public bool? uneditable { get; set; }
        public string correctioncomment { get; set; }
        public bool? addnew { get; set; }
        public string polltype { get; set; }
    }

    public class LinkedforumType
    {
        public string title { get; set; }
        public string linkedforum_index { get; set; }
        public string linkdata_index { get; set; }
        public string required_subtype { get; set; }
    }

    public class Rankinfo
    {
        public string prettyname { get; set; }
        public string shortprettyname { get; set; }
        public string veryshortprettyname { get; set; }
        public string subdomain { get; set; }
        public string rankobjecttype { get; set; }
        public int rankobjectid { get; set; }
        public string rank { get; set; }
        public string baverage { get; set; }
    }

    public class Best
    {
        public int min { get; set; }
        public int max { get; set; }
    }

    public class Recommended
    {
        public int min { get; set; }
        public int? max { get; set; }
    }

    public class Userplayers
    {
        public List<Best> best { get; set; }
        public List<Recommended> recommended { get; set; }
        public string totalvotes { get; set; }
    }

    public class Boardgameweight
    {
        public double averageweight { get; set; }
        public string votes { get; set; }
    }

    public class Polls
    {
        public Userplayers userplayers { get; set; }
        public string playerage { get; set; }
        public string languagedependence { get; set; }
        public string subdomain { get; set; }
        public Boardgameweight boardgameweight { get; set; }
    }

    public class Stats
    {
        public string usersrated { get; set; }
        public string average { get; set; }
        public string baverage { get; set; }
        public string stddev { get; set; }
        public string avgweight { get; set; }
        public string numweights { get; set; }
        public string numgeeklists { get; set; }
        public string numtrading { get; set; }
        public string numwanting { get; set; }
        public string numwish { get; set; }
        public string numowned { get; set; }
        public string numprevowned { get; set; }
        public string numcomments { get; set; }
        public string numwishlistcomments { get; set; }
        public string numhasparts { get; set; }
        public string numwantparts { get; set; }
        public string views { get; set; }
        public string playmonth { get; set; }
        public string numplays { get; set; }
        public string numplays_month { get; set; }
        public int numfans { get; set; }
    }

    public class Relatedcounts
    {
        public int news { get; set; }
        public int blogs { get; set; }
        public int weblink { get; set; }
        public int podcast { get; set; }
    }

    public class Versioninfo
    {
        public string kickstarter_widget_url { get; set; }
        public object gamepageorderurl { get; set; }
        public object shopifyitem { get; set; }
    }

    public class Boardgamedesigner
    {
        public string name { get; set; }
        public string objecttype { get; set; }
        public string objectid { get; set; }
        public int primarylink { get; set; }
        public string itemstate { get; set; }
        public string href { get; set; }
    }

    public class Boardgameartist
    {
        public string name { get; set; }
        public string objecttype { get; set; }
        public string objectid { get; set; }
        public int primarylink { get; set; }
        public string itemstate { get; set; }
        public string href { get; set; }
    }

    public class Boardgamepublisher
    {
        public string name { get; set; }
        public string objecttype { get; set; }
        public string objectid { get; set; }
        public int primarylink { get; set; }
        public string itemstate { get; set; }
        public string href { get; set; }
    }

    public class Boardgamecategory
    {
        public string name { get; set; }
        public string objecttype { get; set; }
        public string objectid { get; set; }
        public int primarylink { get; set; }
        public string itemstate { get; set; }
        public string href { get; set; }
    }

    public class Boardgameversion
    {
        public string name { get; set; }
        public string objecttype { get; set; }
        public string objectid { get; set; }
        public int primarylink { get; set; }
        public string itemstate { get; set; }
        public string href { get; set; }
    }

    public class Boardgameintegration
    {
        public string name { get; set; }
        public string objecttype { get; set; }
        public string objectid { get; set; }
        public int primarylink { get; set; }
        public string itemstate { get; set; }
        public string href { get; set; }
    }

    public class Reimplementation
    {
        public string name { get; set; }
        public string objecttype { get; set; }
        public string objectid { get; set; }
        public int primarylink { get; set; }
        public string itemstate { get; set; }
        public string href { get; set; }
    }

    public class Boardgamesubdomain
    {
        public string name { get; set; }
        public string objecttype { get; set; }
        public string objectid { get; set; }
        public int primarylink { get; set; }
        public string itemstate { get; set; }
        public string href { get; set; }
    }

    public class Links
    {
        public List<Boardgamedesigner> boardgamedesigner { get; set; }
        public List<Boardgameartist> boardgameartist { get; set; }
        public List<Boardgamepublisher> boardgamepublisher { get; set; }
        public List<object> boardgamehonor { get; set; }
        public List<Boardgamecategory> boardgamecategory { get; set; }
        public List<object> boardgamemechanic { get; set; }
        public List<object> boardgameexpansion { get; set; }
        public List<Boardgameversion> boardgameversion { get; set; }
        public List<object> expandsboardgame { get; set; }
        public List<Boardgameintegration> boardgameintegration { get; set; }
        public List<object> contains { get; set; }
        public List<object> containedin { get; set; }
        public List<Reimplementation> reimplementation { get; set; }
        public List<object> reimplements { get; set; }
        public List<object> boardgamefamily { get; set; }
        public List<object> videogamebg { get; set; }
        public List<Boardgamesubdomain> boardgamesubdomain { get; set; }
        public List<object> boardgameaccessory { get; set; }
    }

    public class Linkcounts
    {
        public int boardgamedesigner { get; set; }
        public int boardgameartist { get; set; }
        public int boardgamepublisher { get; set; }
        public int boardgamehonor { get; set; }
        public int boardgamecategory { get; set; }
        public int boardgamemechanic { get; set; }
        public int boardgameexpansion { get; set; }
        public int boardgameversion { get; set; }
        public int expandsboardgame { get; set; }
        public int boardgameintegration { get; set; }
        public int contains { get; set; }
        public int containedin { get; set; }
        public int reimplementation { get; set; }
        public int reimplements { get; set; }
        public int boardgamefamily { get; set; }
        public int videogamebg { get; set; }
        public int boardgamesubdomain { get; set; }
        public int boardgameaccessory { get; set; }
    }

    public class Primaryname
    {
        public string nameid { get; set; }
        public string name { get; set; }
        public string sortindex { get; set; }
        public string primaryname { get; set; }
        public string translit { get; set; }
    }

    public class Website
    {
        public string url { get; set; }
        public string title { get; set; }
    }

    public class Images
    {
        public string thumb { get; set; }
        public string micro { get; set; }
        public string square { get; set; }
        public string squarefit { get; set; }
        public string tallthumb { get; set; }
        public string previewthumb { get; set; }
        public string square200 { get; set; }
    }

    public class Item
    {
        public List<Itemdata> itemdata { get; set; }
        public List<string> relatedlinktypes { get; set; }
        public List<LinkedforumType> linkedforum_types { get; set; }
        public string subtypename { get; set; }
        public List<Rankinfo> rankinfo { get; set; }
        public Polls polls { get; set; }
        public Stats stats { get; set; }
        public Relatedcounts relatedcounts { get; set; }
        public int itemid { get; set; }
        public string objecttype { get; set; }
        public int objectid { get; set; }
        public string label { get; set; }
        public string labelpl { get; set; }
        public string href { get; set; }
        public string subtype { get; set; }
        public List<string> subtypes { get; set; }
        public Versioninfo versioninfo { get; set; }
        public string name { get; set; }
        public object alternatename { get; set; }
        public string yearpublished { get; set; }
        public string minplayers { get; set; }
        public string maxplayers { get; set; }
        public string minplaytime { get; set; }
        public string maxplaytime { get; set; }
        public string minage { get; set; }
        public int override_rankable { get; set; }
        public string targetco_url { get; set; }
        public object instructional_videoid { get; set; }
        public object summary_videoid { get; set; }
        public object playthrough_videoid { get; set; }
        public Links links { get; set; }
        public Linkcounts linkcounts { get; set; }
        public int secondarynamescount { get; set; }
        public int alternatenamescount { get; set; }
        public Primaryname primaryname { get; set; }
        public string description { get; set; }
        public string wiki { get; set; }
        public Website website { get; set; }
        public string imageid { get; set; }
        public Images images { get; set; }
        public string imagepagehref { get; set; }
        public string imageurl { get; set; }
        public string topimageurl { get; set; }
        public string itemstate { get; set; }
    }

    public class Images2
    {
        public int numitems { get; set; }
    }

    public class Videos
    {
        public int numitems { get; set; }
    }

    public class Files
    {
        public string numitems { get; set; }
    }

    public class Media
    {
        public Images2 images { get; set; }
        public Videos videos { get; set; }
        public Files files { get; set; }
    }

    public class Gallery
    {
        public string type { get; set; }
        public string name { get; set; }
    }

    public class Videogalleries
    {
        public List<Gallery> galleries { get; set; }
    }

    public class RootObject
    {
        public Item item { get; set; }
        public Media media { get; set; }
        public Videogalleries videogalleries { get; set; }
    }
}
