﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using HtmlAgilityPack;
using Newtonsoft.Json;

namespace WebScrapper
{
    class Program
    {
        static void Main(string[] args)
        {
            var doc = new HtmlDocument();
            var html = string.Empty;
            var url = "https://boardgamegeek.com/collection/user/PegaKrakow?own=1&subtype=boardgame&ff=1&fbclid=IwAR29ZflpQugEbkLDsxEkrXkyNC2s3qBxDl2mvYWfEfYfmQiRUfnHHlXSGe0";
            var baseUrl = "https://boardgamegeek.com";

            var request = (HttpWebRequest)WebRequest.Create(url);

            List<Game> games = new List<Game>();


            using (var client = new WebClient())
            {
                html = client.DownloadString(url);
                doc.LoadHtml(html);
                var links = new List<string>();
                for (var i = 1; i <= 303; i++)
                {
                    var node = doc.DocumentNode.SelectSingleNode($"//*[@id=\"results_objectname{i}\"]/a");
                    if (node != null)
                    {
                        links.Add(node.GetAttributeValue("href", string.Empty));
                    }
                }
                Console.Write("lista = [");
                links.ForEach(l =>
                {
                    Console.Write($"\"{baseUrl + l}\", ");
                });
                Console.Write("]");


//                games = links.Select(l =>
//                {
//                    var gameUrl = baseUrl + l;
//                    var requestGame = (HttpWebRequest) WebRequest.Create(gameUrl);
//
//                    var gameDoc = new HtmlDocument();
//                    var page = client.DownloadString(gameUrl);
//                    gameDoc.LoadHtml(page);
//                    var raw = gameDoc.ParsedText;
//                    var listOfChars = raw.Split("\n")
//                        .FirstOrDefault(x => x.Contains("GEEK.geekitemPreload = "))
//                        .Split("GEEK.geekitemPreload = ")[1].SkipLast(1);
//                    var rawJson = string.Concat(listOfChars);
//                    var rootObject = JsonConvert.DeserializeObject<RootObject>(rawJson);
//
//                    return new Game
//                    {
////                        ImageUrl = doc.DocumentNode
////                            .SelectSingleNode(
////                                "//*[@id=\"mainbody\"]/div/div[1]/div[1]/div[2]/ng-include/div/ng-include/div/div/div[1]/div/a[1]/img")
////                            .GetAttributeValue("src", string.Empty)
//                    };
//                }).ToList();


            }

            Console.ReadKey();
        }
    }
}
