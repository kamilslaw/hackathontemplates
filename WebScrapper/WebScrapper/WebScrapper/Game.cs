﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebScrapper
{
    public class Game
    {
        public string ImageUrl { get; set; }
        public string Rating { get; set; }
        public string Title { get; set; }
        public string Players { get; set; }
        public string PlayTime { get; set; }
        public string Age { get; set; }
        public string Weight { get; set; }
        public string Description { get; set; }
    }
}
