if [[ $(docker ps -q) ]]; then
    docker kill $(docker ps -q)
fi

docker build --no-cache -t kamilslaw/cybersecuritychat -f api.dockerfile .
docker run -d -p 80:80 kamilslaw/cybersecuritychat