import Vue from "vue";
import { firestorePlugin } from "vuefire";

const firebase = require("firebase");
// Required for side-effects
require("firebase/firestore");

const config = {
  apiKey: "AIzaSyBilgODOFDsTg_8EC6nbLoo2kueAOJhggs",
  authDomain: "hakatun-cebe0.firebaseapp.com",
  databaseURL: "https://hakatun-cebe0.firebaseio.com",
  projectId: "hakatun-cebe0",
  storageBucket: "hakatun-cebe0.appspot.com",
  messagingSenderId: "251228664978"
};
firebase.initializeApp(config);


const db = firebase.firestore();
// const storage = firebase.app().storage("gs://hackathon-88f9d.appspot.com");

Vue.use(firestorePlugin);
Vue.prototype.db = db;
// Vue.prototype.storage = storage;

export default {
  db
};
