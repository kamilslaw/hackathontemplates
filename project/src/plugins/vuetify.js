import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify, {
  theme: {
    primary: '#ee44aa',
    secondary: '#424242',
    accent: '#82B1FF',
    error: '#D91C29',
    info: '#295ED9',
    blue: '#295ED9',
    magenta: '#A9085B',
    success: '#20AA50',
    warning: '#FD6000',
    gold: '#CC8914',
    content: '#10132E',
    gray: '#F2F3F4',
    teal: "#179595",
    labels: '#626475',
    borders: '#cacdd6',
    secondary_background: '#F2F3F4',
    background: '#ffffff'
  },
  options: {
    customProperties: true
  },
  iconfont: 'mdi'
});
