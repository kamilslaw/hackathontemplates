import Vue from "vue";
import Vuex from "vuex";
import firebase from "./plugins/firebase.js";
import axios from "axios";
Vue.use(Vuex);

const API_ADDRESS = "http://localhost:50397/";

export default new Vuex.Store({
  state: {
    userId: localStorage.getItem("userId"),
    drawerShown: false,
    searchBarState: false,
    games: [],
    bookings: [],
    loadingGames: false,
    date: new Date().toISOString().substr(0, 10),
    searchTitles: "",
    searchPlayers: "",
    searchDifficulties: "",
    recommended: false
  },
  mutations: {
    SET_USER_ID(state, userId) {
      state.userId = userId;
      localStorage.setItem("userId", userId);
    },
    TOGGLE_DRAWER(state) {
      state.drawerShown = !state.drawerShown;
    },
    SET_DRAWER_STATE(state, drawerState) {
      state.drawerShown = drawerState;
    },
    TOGGLE_SEARCH_BAR(state) {
      state.searchBarState = !state.searchBarState;
    },
    SET_SEARCH_BAR_STATE(state, searchState) {
      state.searchBarState = searchState;
    },
    SET_DATE(state, date) {
      state.date = date;
    },
    SET_GAMES(state, games) {
      state.games = games;
    },
    SET_LOADING_STATE(state, loading) {
      state.loadingGames = loading;
    },
    SEARCH_TITLES(state, query) {
      state.searchTitles = query;
    },
    SEARCH_PLAYERS(state, query) {
      state.searchPlayers = query;
    },
    SEARCH_DIFFICULTIES(state, query) {
      state.searchDifficulties = query;
    },
    SET_BOOKINGS(state, bookings) {
      state.bookings = bookings;
    },
    SET_RECOMMENDED(state, recomm) {
      state.recommended = recomm;
    }
  },
  actions: {
    checkUsername({ commit }, username) {
      return firebase.db
        .collection("users")
        .where("name", "==", username)
        .get()
        .then(response => {
          response.forEach(document => {
            commit("SET_USER_ID", document.id);
          });
          return response;
        });
    },
    getGames({ state, commit }) {
      const endpoint = API_ADDRESS + `api/GetGames?date=${state.date}`;
      commit("SET_GAMES", []);
      return axios.get(endpoint).then(response => {
        commit("SET_GAMES", response.data);
      });
    },
    reportGame(ctx, { comment, gameId }) {
      const endpoint = API_ADDRESS + "api/ReportGame";
      return axios.post(
        endpoint,
        {
          comment,
          gameId
        },
        {
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Content-Type": "application/x-www-form-urlencoded"
          }
        }
      );
    },
    bookGame({ state }, gameId) {
      const endpoint = API_ADDRESS + "api/CreateReservation";

      let isPrimary = true;
      Object.keys(state.bookings).forEach(key => {
        let booking = state.bookings[key][0];
        if (booking.booking_date.substr(0, 10) === state.date) {
          isPrimary = false;
        }
      });
      return axios.post(endpoint, {
        user: state.userId,
        game: gameId,
        bookingDate: state.date,
        creationDate: new Date().toISOString(),
        isPrimary,
        returnDate: state.date
      });
    },
    getBookings({ state, commit }) {
      const endpoint =
        API_ADDRESS + `api/GetBookingsForUser?userId=${state.userId}`;
      return axios.get(endpoint).then(response => {
        const dayMap = {};
        response.data.forEach(v => {
          if (dayMap[v.day_of_week]) {
            dayMap[v.day_of_week].push(v);
          } else {
            dayMap[v.day_of_week] = [v];
          }
        });
        commit("SET_BOOKINGS", dayMap);
      });
    },
    removeBooking(ctx, bookingId) {
      const endpoint =
        API_ADDRESS + `api/RemoveReservation?bookingId=${bookingId}`;
      return axios.post(endpoint).then(response => {
        console.log(response);
      });
    }
  }
});
