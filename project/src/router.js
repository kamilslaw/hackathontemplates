import Vue from "vue";
import Router from "vue-router";
import SignInForm from "./views/SignInForm.vue";

import GamesListView from "./views/GamesListView.vue";
import AlertToolbar from "./components/AlertToolbar";
import AppToolbar from "./components/AppToolbar";
import GamePanel from "./components/GameListView/GamePanel";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      components: {
        default: GamesListView,
        alert: AlertToolbar,
        toolbar: AppToolbar,
        drawer: GamePanel
      }
    },
    {
      path: "/signIn",
      name: "signIn",
      components: {
        default: SignInForm,
        toolbar: null,
        drawer: null
      }
    },
    {
      path: "/games",
      components: {
        default: GamesListView,
        toolbar: AppToolbar,
        drawer: GamePanel
      }
    }
  ]
});

router.beforeEach((to, from, next) => {
  const userId = localStorage.getItem("userId");
  if (userId && to.name === "signIn") {
    next("/");
  } else if (!userId && to.name !== "signIn") {
    next("/signIn");
  } else {
    next();
  }
});

export default router;
