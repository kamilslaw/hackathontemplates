# import dryscrape
# from bs4 import BeautifulSoup

# my_url = "file:///D:/Projects/Visual%20Studio/Hackaton/testPage.html"
# session = dryscrape.Session()
# session.visit(my_url)
# response = session.body()
# soup = BeautifulSoup(response)
# soup.find(id="intro-text")
from selenium import webdriver
import json
import time
class Game:
    image_src = ''
    rating = ''
    title = ''
    game_length = ''
    players = ''
    age = ''
    complexity = ''
    description = ''

lista = ["https://boardgamegeek.com/boardgame/86073/5-second-rule", "https://boardgamegeek.com/boardgame/432/6-nimmt", "https://boardgamegeek.com/boardgame/68448/7-wonders", "https://boardgamegeek.com/boardgame/173346/7-wonders-duel", "https://boardgamegeek.com/boardgameexpansion/111661/7-wonders-cities", "https://boardgamegeek.com/boardgameexpansion/92539/7-wonders-leaders", "https://boardgamegeek.com/boardgame/172818/above-and-below", "https://boardgamegeek.com/boardgame/202408/adrenaline", "https://boardgamegeek.com/boardgame/200680/agricola-revised-edition", "https://boardgamegeek.com/boardgame/161970/alchemists", "https://boardgamegeek.com/boardgame/6249/alhambra", "https://boardgamegeek.com/boardgame/150018/alternatywy-4", "https://boardgamegeek.com/boardgame/230802/azul", "https://boardgamegeek.com/boardgame/3955/bang", "https://boardgamegeek.com/boardgame/143741/bang-dice-game", "https://boardgamegeek.com/boardgame/219513/barenpark", "https://boardgamegeek.com/boardgame/129492/bee-alert", "https://boardgamegeek.com/boardgame/2453/blokus", "https://boardgamegeek.com/boardgame/170216/blood-rage", "https://boardgamegeek.com/boardgame/180593/bloody-inn", "https://boardgamegeek.com/boardgame/172308/broom-service", "https://boardgamegeek.com/boardgame/184921/bunny-kingdom", "https://boardgamegeek.com/boardgame/155362/cah-n-guns-second-edition", "https://boardgamegeek.com/boardgame/171499/cacao", "https://boardgamegeek.com/boardgame/153938/camel", "https://boardgamegeek.com/boardgame/822/carcassonne", "https://boardgamegeek.com/boardgame/84876/castles-burgundy", "https://boardgamegeek.com/boardgame/155426/castles-mad-king-ludwig", "https://boardgamegeek.com/boardgame/13/catan", "https://boardgamegeek.com/boardgameexpansion/926/catan-cities-knights", "https://boardgamegeek.com/boardgame/102794/caverna-cave-farmers", "https://boardgamegeek.com/boardgame/175117/celestia", "https://boardgamegeek.com/boardgame/33088/chabyrinthe", "https://boardgamegeek.com/boardgame/172287/champions-midgard", "https://boardgamegeek.com/boardgame/178900/codenames", "https://boardgamegeek.com/boardgame/158899/colt-express", "https://boardgamegeek.com/boardgame/124361/concordia", "https://boardgamegeek.com/boardgame/39463/cosmic-encounter", "https://boardgamegeek.com/boardgame/521/crokinole", "https://boardgamegeek.com/boardgame/143986/cv", "https://boardgamegeek.com/boardgame/54998/cyclades", "https://boardgamegeek.com/boardgame/150376/dead-winter-crossroads-game", "https://boardgamegeek.com/boardgame/225694/decrypto", "https://boardgamegeek.com/boardgame/104162/descent-journeys-dark-second-edition", "https://boardgamegeek.com/boardgame/91312/discworld-ankh-morpork", "https://boardgamegeek.com/boardgame/92828/dixit-odyssey", "https://boardgamegeek.com/boardgame/92828/dixit-odyssey", "https://boardgamegeek.com/boardgame/209418/dominion-second-edition", "https://boardgamegeek.com/boardgame/194880/dream-home", "https://boardgamegeek.com/boardgame/72125/eclipse", "https://boardgamegeek.com/boardgame/146021/eldritch-horror", "https://boardgamegeek.com/boardgame/163968/elysium", "https://boardgamegeek.com/boardgame/198138/enchanters", "https://boardgamegeek.com/boardgame/155703/evolution", "https://boardgamegeek.com/boardgame/172225/exploding-kittens", "https://boardgamegeek.com/boardgame/177736/feast-odin", "https://boardgamegeek.com/boardgame/157354/five-tribes", "https://boardgamegeek.com/boardgame/100901/flash-point-fire-rescue", "https://boardgamegeek.com/boardgame/169124/flick-em", "https://boardgamegeek.com/boardgame/65244/forbidden-island", "https://boardgamegeek.com/boardgame/171273/fuse", "https://boardgamegeek.com/boardgame/31481/galaxy-trucker", "https://boardgamegeek.com/boardgame/103343/game-thrones-board-game-second-edition", "https://boardgamegeek.com/boardgame/162823/el-gaucho", "https://boardgamegeek.com/boardgame/143693/glass-road", "https://boardgamegeek.com/boardgame/182874/grand-austria-hotel", "https://boardgamegeek.com/boardgame/146886/la-granja", "https://boardgamegeek.com/boardgame/22198/great-wall-china", "https://boardgamegeek.com/boardgame/193738/great-western-trail", "https://boardgamegeek.com/boardgame/132372/guildhall", "https://boardgamegeek.com/boardgame/98778/hanabi", "https://boardgamegeek.com/boardgame/35677/le-havre", "https://boardgamegeek.com/boardgame/2655/hive", "https://boardgamegeek.com/boardgame/154203/imperial-settlers", "https://boardgamegeek.com/boardgame/31594/year-dragon", "https://boardgamegeek.com/boardgame/63888/innovation", "https://boardgamegeek.com/boardgame/176494/isle-skye-chieftain-king", "https://boardgamegeek.com/boardgame/148949/istanbul", "https://boardgamegeek.com/boardgame/54043/jaipur", "https://boardgamegeek.com/boardgame/8098/jungle-speed", "https://boardgamegeek.com/boardgame/200147/kanagawa", "https://boardgamegeek.com/boardgame/183251/karuba", "https://boardgamegeek.com/boardgame/127023/kemet", "https://boardgamegeek.com/boardgame/70323/king-tokyo", "https://boardgamegeek.com/boardgame/107529/kingdom-builder", "https://boardgamegeek.com/boardgame/204583/kingdomino", "https://boardgamegeek.com/boardgame/85325/kolejka", "https://boardgamegeek.com/boardgame/96913/lancaster", "https://boardgamegeek.com/boardgame/97842/last-will", "https://boardgamegeek.com/boardgame/125618/libertalia", "https://boardgamegeek.com/boardgame/134453/little-prince-make-me-planet", "https://boardgamegeek.com/boardgame/110327/lords-waterdeep", "https://boardgamegeek.com/boardgame/129622/love-letter", "https://boardgamegeek.com/boardgame/196326/love-letter-premium", "https://boardgamegeek.com/boardgame/96848/mage-knight-board-game", "https://boardgamegeek.com/boardgame/41916/magic-labyrinth", "https://boardgamegeek.com/boardgame/205059/mansions-madness-second-edition", "https://boardgamegeek.com/boardgame/139030/mascarade", "https://boardgamegeek.com/boardgame/176920/mission-red-planet-second-edition", "https://boardgamegeek.com/boardgame/172386/mombasa", "https://boardgamegeek.com/boardgame/21763/mr-jack", "https://boardgamegeek.com/boardgame/191004/my-first-stone-age", "https://boardgamegeek.com/boardgame/183840/oh-my-goods", "https://boardgamegeek.com/boardgame/147949/one-night-ultimate-werewolf", "https://boardgamegeek.com/boardgame/106753/ooga-booga", "https://boardgamegeek.com/boardgame/70149/ora-et-labora", "https://boardgamegeek.com/boardgame/164928/orleans", "https://boardgamegeek.com/boardgame/30549/pandemic", "https://boardgamegeek.com/boardgame/163412/patchwork", "https://boardgamegeek.com/boardgame/218603/photosynthesis", "https://boardgamegeek.com/boardgame/2651/power-grid", "https://boardgamegeek.com/boardgame/555/princes-florence", "https://boardgamegeek.com/boardgame/3076/puerto-rico", "https://boardgamegeek.com/boardgame/176396/quadropolis", "https://boardgamegeek.com/boardgame/161417/red7", "https://boardgamegeek.com/boardgame/128882/resistance-avalon", "https://boardgamegeek.com/boardgame/9441/ribbit", "https://boardgamegeek.com/boardgame/205896/rising-sun", "https://boardgamegeek.com/boardgame/144733/russian-railroads", "https://boardgamegeek.com/boardgame/9220/saboteur", "https://boardgamegeek.com/boardgameexpansion/91072/saboteur-2-expansion-only-editions", "https://boardgamegeek.com/boardgame/169786/scythe", "https://boardgamegeek.com/boardgame/108745/seasons", "https://boardgamegeek.com/boardgame/157969/sheriff-nottingham", "https://boardgamegeek.com/boardgame/40692/small-world", "https://boardgamegeek.com/boardgame/38453/space-alert", "https://boardgamegeek.com/boardgame/148228/splendor", "https://boardgamegeek.com/boardgameexpansion/220653/splendor-cities-splendor", "https://boardgamegeek.com/boardgame/63268/spot-it", "https://boardgamegeek.com/boardgame/34635/stone-age", "https://boardgamegeek.com/boardgame/123260/suburbia", "https://boardgamegeek.com/boardgame/146508/time-stories", "https://boardgamegeek.com/boardgame/1111/taboo", "https://boardgamegeek.com/boardgame/113997/tajemnicze-domostwo", "https://boardgamegeek.com/boardgame/70919/takenoko", "https://boardgamegeek.com/boardgame/27627/talisman-revised-4th-edition", "https://boardgamegeek.com/boardgame/24508/taluva", "https://boardgamegeek.com/boardgame/120677/terra-mystica", "https://boardgamegeek.com/boardgame/167791/terraforming-mars", "https://boardgamegeek.com/boardgame/182028/through-ages-new-story-civilization", "https://boardgamegeek.com/boardgame/215/tichu", "https://boardgamegeek.com/boardgameexpansion/106637/ticket-ride-map-collection-volume-1-team-asia-lege", "https://boardgamegeek.com/boardgame/14996/ticket-ride-europe", "https://boardgamegeek.com/boardgame/218208/ticket-ride-first-journey-europe", "https://boardgamegeek.com/boardgame/1353/times", "https://boardgamegeek.com/boardgame/38713/times-edicion-amarilla", "https://boardgamegeek.com/boardgame/85256/timeline-inventions", "https://boardgamegeek.com/boardgame/203655/timeline-polska", "https://boardgamegeek.com/boardgame/123540/tokaido", "https://boardgamegeek.com/boardgame/73439/troyes", "https://boardgamegeek.com/boardgame/5894/twister", "https://boardgamegeek.com/boardgame/126163/tzolk-mayan-calendar"]

browser = webdriver.Chrome("D:\Downloads\chromedriver_win32 (1)\chromedriver.exe") #replace with .Firefox(), or with the browser of your choice
url = "https://boardgamegeek.com/boardgame/86073/5-second-rule"
games = []
for elem in lista:
    browser = webdriver.Chrome("D:\Downloads\chromedriver_win32 (1)\chromedriver.exe") #replace with .Firefox(), or with the browser of your choice
    browser.get(elem) #navigate to the page
    game = Game()
    elem = browser.find_element_by_xpath('//*[@id="mainbody"]/div/div[1]/div[1]/div[2]/ng-include/div/ng-include/div/div/div[1]/div/a[1]/img')
    game.image_src = elem.get_attribute("src")

    elem = browser.find_element_by_xpath('//*[@id="mainbody"]/div/div[1]/div[1]/div[2]/ng-include/div/ng-include/div/div/div[2]/div[1]/div/div[1]/overall-rating/div/div/a/span[1]')

    game.rating = elem.text.strip()
    elem = browser.find_element_by_xpath('//*[@id="mainbody"]/div/div[1]/div[1]/div[2]/ng-include/div/ng-include/div/div/div[2]/div[1]/div/div[2]/h1/a')

    game.title = elem.text.strip()
    elem = browser.find_element_by_xpath('//*[@id="mainbody"]/div/div[1]/div[1]/div[2]/ng-include/div/ng-include/div/div/div[2]/div[2]/gameplay-module/div/div/ul/li[2]/div[1]/span')

    game.game_length = elem.text.strip()
    elem = browser.find_element_by_xpath('//*[@id="mainbody"]/div/div[1]/div[1]/div[2]/ng-include/div/ng-include/div/div/div[2]/div[2]/gameplay-module/div/div/ul/li[1]/div[1]/span')

    game.players = elem.text.strip()
    elem = browser.find_element_by_xpath('//*[@id="mainbody"]/div/div[1]/div[1]/div[2]/ng-include/div/ng-include/div/div/div[2]/div[2]/gameplay-module/div/div/ul/li[3]/div[1]/span')

    game.age = elem.text.strip()
    elem = browser.find_element_by_xpath('//*[@id="mainbody"]/div/div[1]/div[1]/div[2]/ng-include/div/ng-include/div/div/div[2]/div[2]/gameplay-module/div/div/ul/li[4]/div[1]/span[2]')

    game.complexity = elem.text.strip()
    elem = browser.find_element_by_xpath('//*[@id="mainbody"]/div/div[1]/div[1]/div[2]/ng-include/div/div/ui-view/ui-view/div[1]/overview-module/description-module/div/div[2]/div/div[3]/div[1]/div[1]/div/article/div/p')

    game.description = elem.text.strip()

    dt = {}
    dt.update(vars(game))

    browser.quit()
    games.append(dt)

print(json.dumps(games))

