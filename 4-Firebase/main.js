// https://firebase.google.com/docs/database/web/structure-data


var config = {
  apiKey: "AIzaSyB_H8evRjqZGapIOh7gXS4cs-kuwRfRRS4",
  authDomain: "hackathon-88f9d.firebaseapp.com",
  databaseURL: "https://hackathon-88f9d.firebaseio.com/",
  storageBucket: "bucket.appspot.com"
};
firebase.initializeApp(config);
var database = firebase.database();


var commentsRef = database.ref('comments/1');


function send() {
  var data = {
    name: document.getElementById('author').value,
    msg: document.getElementById('msg').value
  }
  commentsRef.push(data);
}


commentsRef.on('child_added', function (data) {
  addCommentElement(data.val());
});

function addCommentElement(data) {
  var wrapper = document.getElementById("wrapper");

  var author = document.createElement('p');
  author.className = 'name';
  author.innerText = data.name;

  var content = document.createElement('p');
  content.innerText = data.msg;

  wrapper.appendChild(author);
  wrapper.appendChild(content);
}