﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CouchDB.Client;
using CouchDB.Client.FluentMango;
using Newtonsoft.Json.Linq;

namespace ConsoleAppCouchDb
{
    // https://github.com/jefersonsv/CouchDB.Client
    class Program
    {
        static void Main(string[] args) => RunProgram().GetAwaiter().GetResult();

        static async Task RunProgram()
        {
            var client = new CouchClient("http://176.107.131.163:5984");
            var db = await client.GetDatabaseAsync("testdb");

            var random = new Random();

            Enumerable.Range(0, 5).ToList().ForEach(async (_) =>
            {
                var doc = new
                {
                    _id = Guid.NewGuid().ToString(),
                    machine = Environment.MachineName,
                    when = DateTime.Now,
                    nr = random.Next(100),
                    randomMsg = Guid.NewGuid().ToString()
                };
                var inserted = await db.InsertAsync(doc);
            });

            var info = await db.GetInfoAsync();
            Console.WriteLine("## INFO ##" + info.Content);

            var find = new FindBuilder().AddSelector("nr", SelectorOperator.GreaterThan, 80);
            var results = await db.FindAsync(find);
            var docs = results.Docs.ToObject<List<DDD>>();

            docs.ForEach(d => Console.WriteLine($"{d._id}: {d.nr} ({d.randomMsg})"));
            docs[0].randomMsg += "_DD_";
            var updateResult = await db.UpdateAsync(JToken.FromObject(docs[0]));

            var msg = (await db.GetAsync(docs[0]._id)).Json.ToObject<DDD>().randomMsg;
            Console.WriteLine(msg);

            Console.WriteLine("## DONE ##");
            Console.ReadLine();
        }
    }

    class DDD
    {
        public string _id { get; set; }
        public string _rev { get; set; }
        public int nr { get; set; }
        public string randomMsg { get; set; }
    }
}
